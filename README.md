TP1 Option RV : Bataille aérienne
=================================

L'objectif est de simuler une bataille aérienne entre 2 escadrilles, la vôtre (i.e. celle que
vous aurez programmée avec votre tactique) et celle contrôlée par l’ordinateur. A chaque, les
avions pourront bouger et tirer.

**Jouez collectif : merci de commenter vos fonctions un maximum !!**

Un tuto GIT : [docs/GitHowTo.md](docs/GitHowTo.md)

Organisation
------------

Organisation des classes : [docs/Classes.md](docs/Classes.md)

Organisation des fonctions : [docs/Fonctions.md](docs/Fonctions.md)

Organisation OSG : [docs/OpenSceneGraph.md](docs/OpenSceneGraph.md)