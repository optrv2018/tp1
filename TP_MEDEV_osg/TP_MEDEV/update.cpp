/*
    Authors
    Chaimaa AAJAFIR
    Jiacheng LIU
    Douaa KHALIL

*/


// il faut penser à definir deux variables (int* nbreAvionCPU, int* nbreAvionJoueur)  et les initialiser a 2


// tester si la partie de jeux est terminee (deux avion du meme types sont detruites)
bool jeuTermine(int* nbreAvionCPU, int* nbreAvionJoueur){
    if((*nbreAvionCPU)==0 || (*nbreAvionCPU)==0){
        return true;
    }
    return false;
}


//mettre à jour le nombre d'avions qui se trouvent sur  le terrain
bool updateNombreAvions(vector<Avion*> listeAvion, int* nbreAvionCPU, int* nbreAvionJoueur){
    // iteration sur les avions
    for(int i=0; i < listeAvion.size(); i++) {
        // tester si une avion est detruite
        if (listeAvion[i]->estDetruit()){
            //tester si avion cpu
            if(listeAvion[i]->estCPU){
                (*nbreAvionCPU)--;
            }
            // si avion joueur
            else
            {
                (*nbreAvionJoueur)--;
            }
        }
    }
}


// faire tourner le jeu jusqu'a ce que une equipe perd
void update(vector<Avion*> listeAvion, int* nbreAvionCPU, int* nbreAvionJoueur){
    // iteration sur les avions
    for(int i=0; i < listeAvion.size(); i++) {
        // si l'avion n'est pas detruite et que le jeu n'est pas termine
        if (!listeAvion[i]->estDetruit() && !jeuTermine()){
            listeAvion[i]->Update();
            // on mets à jour le nombre d'avions se trouvant sur le terrain
            updateNombreAvions(listeAvion,nbreAvionCPU,nbreAvionJoueur);
        }
    }
}





