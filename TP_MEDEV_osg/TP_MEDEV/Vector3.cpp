#include "Vector3.h"
#include <cmath>

Vector3::Vector3()
{
    x = 0;
    y = 0;
    z = 0;
}

Vector3::Vector3(double x,double y,double z=0)
{
    this->x = x;
    this->y = y;
    this->z = z;
}

Vector3::Vector3(const Vector3 & v)
{
    setX(v.getX());
    setY(v.getY());
    setZ(v.getZ());
}

Vector3::Vector3(const Vector3 & from,const Vector3 & to)
{
    x = to.getX() - from.getX();
    y = to.getY() - from.getY();
    z = to.getZ() - from.getZ();
}

Vector3 & Vector3::operator= (const Vector3 & v)
{
    x = v.getX();
    y = v.getY();
    z = v.getZ();
    return *this;
}

bool Vector3::operator== (const Vector3 & v) const{
	return (v.getX() == x &&  v.getY() == y && v.getZ() == z);
}

Vector3 & Vector3::operator+= (const Vector3 & v)
{
    x += v.getX();
    y += v.getY();
    z += v.getZ();
    return *this;
}

Vector3 Vector3::operator+ (const Vector3 & v) const
{
    Vector3 t = *this;
    t += v;
    return t;
}

Vector3 & Vector3::operator-= (const Vector3 & v)
{
    x -= v.getX();
    y -= v.getY();
    z -= v.getZ();
    return *this;
}

Vector3 Vector3::operator- (const Vector3 & v) const
{
    Vector3 t = *this;
    t -= v;
    return t;
}

Vector3 & Vector3::operator*= (const double a)
{
    x *= a;
    y *= a;
    z *= a;
    return *this;
}

Vector3 Vector3::operator* (const double a)const
{
    Vector3 t = *this;
    t *= a;
    return t;
}

Vector3 operator* (const double a,const Vector3 & v)
{
    return Vector3(v.getX()*a,v.getY()*a,v.getZ()*a);
}

Vector3 & Vector3::operator/= (const double a)
{
    x /= a;
    y /= a;
    z /= a;
    return *this;
}

Vector3 Vector3::operator/ (const double a)const
{
    Vector3 t = *this;
    t /= a;
    return t;
}

// Setters/Getters
void Vector3::setX(double x) {
   this->x = x;
}

double Vector3::getX() const {
   return x;
}

void Vector3::setY(double y) {
   this->y = y;
}

double Vector3::getY() const{
   return y;
}

void Vector3::setZ(double z) {
   this->z = z;
}

double Vector3::getZ() const{
   return z;
}

Vector3 Vector3::crossProduct(const Vector3 & v)const
{
    Vector3 t;
    t.setX(y*v.getZ() - z*v.getY());
    t.setY(z*v.getX() - x*v.getZ());
    t.setZ(x*v.getY() - y*v.getX());
    return t;
}

double Vector3::length()const
{
    return sqrt( x*x + y*y + z*z);
}

Vector3  Vector3::normalized() const
{
    return (*this) / length();
  
}
