///OLIVIER CORNET
///Avion.h


#pragma once

#include "Vector3.h"
#include <vector>
#include <string>

#define TAILLE 14

class Avion
{
protected :
	Vector3 position;
	Vector3 direction;
	int delaiRechargement;
	bool detruit;
	std::string nomDeCode; //alpha pour les joueurs, bravo pour les CPU

public:
	Avion();
	Avion(Vector3 position, Vector3 direction);
	void Update(std::vector<Avion*> listeAvion); //A besoin des positions et �tats des autres avions pour 
	virtual void Action(std::vector<Avion*> listeAvion) = 0; //Contr�le de l'avion (changement de cap et tir) 
	bool estCPU() const;

	bool Tire(std::vector<Avion*> listeAvion); //Renvoie true si a tir�

	
	void setPosition(Vector3 p);
	Vector3 getPosition() const;

	void setDirection(Vector3 d);
	Vector3 getDirection() const;

	void Destroy();
	bool estDetruit() const;

	void setNomDeCode(std::string n);
	std::string getNomDeCode() const;

	static bool HorsDuCube(Vector3 v); //Si la position en param�tre est hors du cube de cot� TAILLE

	
	~Avion(void);
};

