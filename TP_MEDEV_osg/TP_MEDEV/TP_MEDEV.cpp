// TP_MEDEV.cpp�: d�finit le point d'entr�e pour l'application console.
//

//Manon et Octave

// TP_OSG.cpp�: d�finit le point d'entr�e pour l'application console.
//

// base
#include <osgViewer/Viewer>
#include <osg/Material>
#include <osg/StateSet>
#include <osg/ShapeDrawable>
#include <vector>
#include <osg/PositionAttitudeTransform>

// Keyboard input
#include <osgViewer/ViewerEventHandlers>
#include <osgGA/StateSetManipulator>
#include <osg/MatrixTransform>
#include <osg/Geometry>
#include <iostream>
#include <osgDB/ReadFile>
#include <osg/Light>
#include <osg/LightSource>
#include <osgDB/ReadFile>
#include <osg/Texture2D>



     


int main()
{
/* OBJECTS CREATION */


	//Creating the viewer	
	osgViewer::Viewer viewer ;

	//Creating the root node
	osg::ref_ptr<osg::Group> root (new osg::Group);
	

//Bullet
	
	osg::ref_ptr<osg::Node> bullet = osgDB::readNodeFile("CHAHIN_ARROW.obj");
	//Create transformation node
	osg::ref_ptr<osg::MatrixTransform> bulletScaleMAT (new osg::MatrixTransform);
	//Scale matrix
	osg::Matrix bulletScaleMatrix;
	bulletScaleMatrix.makeScale(osg::Vec3f(0.5f,0.5f,0.5f));
	bulletScaleMAT->addChild(bullet.get());
	bulletScaleMAT->setMatrix(bulletScaleMatrix);

//Our_Plane
	
	osg::ref_ptr<osg::Node> OurPlane = osgDB::readNodeFile("PUSHILIN_Plane.obj");
	//Create transformation node
	osg::ref_ptr<osg::MatrixTransform> OurPlaneScaleMAT (new osg::MatrixTransform);
	//Scale matrix
	osg::Matrix OurPlaneScaleMatrix;
	OurPlaneScaleMatrix.makeScale(osg::Vec3f(1.0f,1.0f,1.0f));
	OurPlaneScaleMAT->addChild(OurPlane.get());
	OurPlaneScaleMAT->setMatrix(OurPlaneScaleMatrix);


//Computer_plane
	
	osg::ref_ptr<osg::Node> ComputerPlane = osgDB::readNodeFile("model.obj");
	//Create transformation node
	osg::ref_ptr<osg::MatrixTransform> ComputerPlaneScaleMAT (new osg::MatrixTransform);
	//Scale matrix
	osg::Matrix ComputerPlaneScaleMatrix;
	ComputerPlaneScaleMatrix.makeScale(osg::Vec3f(3.0f,3.0f,3.0f));
	ComputerPlaneScaleMAT->addChild(ComputerPlane.get());
	ComputerPlaneScaleMAT->setMatrix(ComputerPlaneScaleMatrix);



//Around 
	
	osg::ref_ptr<osg::Node> around = osgDB::readNodeFile("CUPIC_HILL.obj");
	//Create transformation node
	osg::ref_ptr<osg::MatrixTransform> AroundScaleMAT (new osg::MatrixTransform);
	//Scale matrix
	osg::Matrix AroundScaleMatrix;
	AroundScaleMatrix.makeScale(osg::Vec3f(0.03f,0.03f,0.03f));
	AroundScaleMAT->addChild(around.get());
	AroundScaleMAT->setMatrix(AroundScaleMatrix);

/* POSITION */

	//translation et rotation

//bullet
	//Create the transformation node
	osg::ref_ptr<osg::PositionAttitudeTransform> bulletTrans (new osg::PositionAttitudeTransform);
	osg::Vec3f objectPosTrans = osg::Vec3f(12.0f,-10.0f,30.0f);
	//Set the transformation parameters
	bulletTrans->setPosition(objectPosTrans);
	//Add the objects to transform
	bulletTrans->addChild(bulletScaleMAT.get());
	bulletTrans->setAttitude(osg::Quat(osg::DegreesToRadians(270.0), osg::Vec3(0,0,0.5)));

//OurPlane
	//Create the transformation node
	osg::ref_ptr<osg::PositionAttitudeTransform> OurPlaneTrans (new osg::PositionAttitudeTransform);
	osg::Vec3f objectPosTransOurPlane = osg::Vec3f(14.0f,6.0f,20.0f);
	//Set the transformation parameters
	OurPlaneTrans->setPosition(objectPosTransOurPlane);
	//Add the objects to transform
	OurPlaneTrans->addChild(OurPlaneScaleMAT.get());
	OurPlaneTrans->setAttitude(osg::Quat(osg::DegreesToRadians(-56.0), osg::Vec3(0,0,1)));

//Computer Plane
	//Create the transformation node
	osg::ref_ptr<osg::PositionAttitudeTransform> ComputerPlaneTrans (new osg::PositionAttitudeTransform);
	osg::Vec3f objectPosTransComputerPlane = osg::Vec3f(2.0f,+2.0f,20.0f);
	//Set the transformation parameters
	ComputerPlaneTrans->setPosition(objectPosTransComputerPlane);
	//Add the objects to transform
	ComputerPlaneTrans->addChild(ComputerPlaneScaleMAT.get());
	ComputerPlaneTrans->setAttitude(osg::Quat(osg::DegreesToRadians(225.0), osg::Vec3(0,0,1)));



//Autour 
	//Create the transformation node
	osg::ref_ptr<osg::PositionAttitudeTransform> AroundTrans (new osg::PositionAttitudeTransform);
	osg::Vec3f objectPosTransAround = osg::Vec3f(0.0f,0.0f,-10.0f);
	//Set the transformation parameters
	AroundTrans->setPosition(objectPosTransAround);
	//Add the objects to transform
	AroundTrans->addChild(AroundScaleMAT.get());
	//AroundPondTrans->setAttitude(osg::Quat(osg::DegreesToRadians(0.0), osg::Vec3(0,0,1)));
	




/* LIGHTING */
		
	//Create nodes
		osg::ref_ptr<osg::Group> lightGroup (new osg::Group);
		osg::ref_ptr<osg::LightSource> lightSource1 = new osg::LightSource;
	//Create a local light
		osg::Vec4f lightPosition (osg::Vec4(1.0f,1.0f, 1.0f, 1.0f));
		 osg::ref_ptr<osg::Light> myLight = new osg::Light;
		myLight->setLightNum(1);
		myLight->setPosition(osg::Vec4(1.0f,1.0f, 1.0f, 1.0f));
		myLight->setAmbient(osg::Vec4(1.0f,1.0f, 1.0f, 1.0f));
		myLight->setDiffuse(osg::Vec4(1.0f,1.0f, 1.0f, 1.0f));
		myLight->setConstantAttenuation(1.0f);
	//Set light source parameters
		lightSource1->setLight(myLight);
		
	//Add to light source group
		lightGroup->addChild(lightSource1.get());
	//Light markers: small spheres

/* SCENE GRAPH*/

	
	
/*Animations*/



//add children to the scene graph root
	root->addChild(bullet);
	root->addChild(OurPlaneTrans);
	root->addChild(ComputerPlaneTrans);
	root->addChild(AroundTrans);
	

	root->addChild(lightSource1.get());



	// Set the scene data
	viewer.setSceneData( root.get() ); 
	


/* KEYBOARD INPUT */
	
 	//Stats Event Handler s key
	viewer.addEventHandler(new osgViewer::StatsHandler);

	//Windows size handler
	viewer.addEventHandler(new osgViewer::WindowSizeHandler);
	
	// add the state manipulator
    	viewer.addEventHandler( new osgGA::StateSetManipulator(viewer.getCamera()->getOrCreateStateSet()) );


		

/* START VIEWER */

	//The viewer.run() method starts the threads and the traversals.
	return (viewer.run());
}
