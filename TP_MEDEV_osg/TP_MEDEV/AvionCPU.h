//AvionCPU.h
//Vivien et L�a

#pragma once

#include "Avion.h"

class AvionCPU: public Avion
{
	public static int id;

	public:
		AvionCPU(Vector3 pos, Vector3 direct);
		void Action(std::vector<Avion*> listeAvion);
}