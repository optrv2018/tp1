#include <Vector>
#include <String>
#include "Avion.h";
#include "AvionJoueur.h"
#include "AvionCPU.h"
#include "Vector3.h"

using namespace std;

void affichage(AvionJoueur* j1, AvionJoueur* j2, AvionCPU* cpu1, AvionCPU* cpu2)
{
  double j1X = j1->getPosition().getX();
  double j1Y = j1->getPosition().getY();

  double j2X = j2->getPosition().getX();
  double j2Y = j2->getPosition().getY();

  double cpu1X = cpu1->getPosition().getX();
  double cpu1Y = cpu1->getPosition().getY();

  double cpu2X = cpu2->getPosition().getX();
  double cpu2Y = cpu2->getPosition().getY();

  for (int i = 0; i < TAILLE; i++)
  {
    for (int j = 0; j < TAILLE; j++)
      {
        int a,b;
        (a,b) = (i,j);

        switch(a,b)
        {
          case (j1X,j1Y): cout << "  j1  ";
              break;

          case (j2X,j2Y) : cout << "  j2  ";
              break;

          case (cpu1X,cpu1Y) : cout << " cpu1 ";
              break;

          case (cpu2X,cpu2Y) : cout << " cpu2 ";
              break;

          default : cout << "   .   ";
              break;
        }

      }
      cout << endl;
  }
}
