#pragma once

#include <iostream>
using namespace std;

class Vector3
{
private:
	double x;
	double y;
	double z;
public:
	
	Vector3();
	Vector3(double x,double y,double z);
	Vector3(const Vector3 & v);
	Vector3(const Vector3 & from,const Vector3 & to);
   
	Vector3 & operator= (const Vector3 & v);

	bool operator== (const Vector3 & v) const;
   
	Vector3 & operator+= (const Vector3 & v);
	Vector3 operator+ (const Vector3 & v) const;
   
	Vector3 & operator-= (const Vector3 & v);
	Vector3 operator- (const Vector3 & v) const;
   
	Vector3 & operator*= (const double a);
	Vector3 operator* (const double a)const;
	friend Vector3 operator* (const double a,const Vector3 & v);
   
	Vector3 & operator/= (const double a);
	Vector3 operator/ (const double a)const;
   
	// Setters/Getters
	void setX(double x);
	double getX() const;
   
	void setY(double y);
	double getY() const;
   
	void setZ(double z);
	double getZ() const;
   
	Vector3 crossProduct(const Vector3 & v)const;
	double length()const;
	Vector3  normalized() const;
   
	friend ostream& operator<<(ostream& os, Vector3 const & v) {
		return os << "("<<v.getX()<<","<<v.getY()<<","<<v.getZ()<<")" << endl;
	}
};

