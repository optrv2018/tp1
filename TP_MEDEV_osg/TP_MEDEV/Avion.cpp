///OLIVIER CORNET
///Avion.cpp


#include "Avion.h"


Avion::Avion(){
	position = Vector3(0,0,0);
	direction = Vector3(1,0,0);
	delaiRechargement = 0;
	detruit = false;
}

Avion::Avion(Vector3 position, Vector3 direction)
{
	this->position = position;
	this->direction = direction;
	delaiRechargement = 0;
	detruit = false;
}

void Avion::Update(std::vector<Avion*> listeAvion){
	//On d�termine la direction et si on tire;
	Action(listeAvion);

	//D�placement en fonction de la direction
	position += direction;

	if(position.getZ() < 0) Destroy(); // Si on rentre dans le sol
	if(!estCPU()){ //Si c'est un avion joueur
		 //D�truire si il sort de la carte
		if(HorsDuCube(position)) Destroy();
	}else{ // Sinon
		//On le t�l�porte � l'autre bout
		if(position.getX() < 0) position.setX(TAILLE-1);
		if(position.getX() > TAILLE-1) position.setX(0);
		if(position.getY() < 0) position.setY(TAILLE-1);
		if(position.getY() > TAILLE-1) position.setY(0);
		//Sauf dans l'axe vertical
		if(position.getZ() < 0) Destroy();
		if(position.getZ() > TAILLE-1) Destroy();
	}

	if(delaiRechargement >0){ //On recharge si on a tir�
		delaiRechargement--;
	}
}


bool Avion::Tire(std::vector<Avion*> listeAvion){
	if(delaiRechargement == 0){
		//Ici, effet de tir

		//Tir
		Vector3 positionBalle = position + direction;
		bool avionTouche = false;
		while(!HorsDuCube(positionBalle) && !avionTouche){
			for(Avion* a : listeAvion){//Pour chaque avion sur la carte
				if(a->getPosition() == positionBalle){ //Si la balle a la m�me position que l'avion
					a->Destroy(); //Alors il est d�truit
					avionTouche = true;
					break;
				}
			}

			positionBalle += direction;//La balle poursuit sa trajectoire
		}


		delaiRechargement = 2; //Attendre le tour actuel + le tour prochain (= 2 tours) avant de tirer � nouveau
		return true;
	}
	return false;
}


void Avion::setPosition(Vector3 p){
	position = p;
}

Vector3 Avion::getPosition() const{
	return position;
}

void Avion::setDirection(Vector3 d){
	direction = d;
}

Vector3 Avion::getDirection() const{
	return direction;
}

bool Avion::estCPU() const{
	if(nomDeCode[0] == 'a' || nomDeCode[0] == 'A'){
		return false;
	}
	else return true;
}

void Avion::Destroy(){
	detruit = true;
	//Ici, animation de destruction
}

bool Avion::estDetruit() const{
	return detruit;
}

void Avion::setNomDeCode(std::string n){
	nomDeCode = n;
}

std::string Avion::getNomDeCode() const{
	return nomDeCode;
}

bool Avion::HorsDuCube(Vector3 v){
	return (v.getX() < 0 || v.getX() > TAILLE-1 || v.getY() < 0 || v.getY() > TAILLE-1 || v.getZ() < 0 || v.getZ() > TAILLE-1);
}

Avion::~Avion(void)
{
}
