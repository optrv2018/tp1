Git How-To
==========

Introduction
------------

**TL;DR GIT est un serveur de fichier en ligne. Pour travailler dessus, on récupère, on fait les modifs et on renvoie avec un message.**

GIT est un gestionnaire de versions en ligne, qui gère du code et qui permet de naviguer dans l’historique à n’importe quel moment.
Quelques généralités :

- Lorsque l’on veut travailler sous git, il faut commencer par “récupérer” les données à jour sur son ordinateur, en local. Ensuite, on peut écrire son code. Une fois que l’on a terminé, il faut “envoyer” les modifications.

- Ne pas oublier que **TOUT LE MONDE TRAVAILLE EN PARALLÈLE** : la version sur le serveur n’est pas forcément la même que vous avez en local !
Il faut donc penser à souvent mettre à jour sa copie locale.

- Lorsque l’on envoie une mise à jour, il faut systématiquement donner une raison. Essayez de la rendre la plus claire et précise possible !
Évitez : “Mise à jour du fichier avion.c”
Préférez : “Implémentation de la fonction vol() dans la classe avion définie dans avion.c”



Comment utiliser GIT ?
----------------------

Un tuto détaillé : [lien](https://www.christopheducamp.com/2013/12/16/github-pour-nuls-partie-2)

### 1. Récupération du repo (si vous n’avez jamais téléchargé le code)

1. Ouvrez l’explorateur dans le dossier parent du dossier de travail (pour la simple et bonne raison que git va créer automatiquement un dossier “TP1”)

2. Faites Shift-Clic droit

3. Cliquez sur “Ouvrir un invite de commande ici”

4. Tapez la commande suivante :

        git clone https://bitbucket.org/OPTRV2018/TP1
        
5. Connectez-vous à votre compte bitbucket pour télécharger…

### 2. Envoi des mises à jour

1. Sauvegardez bien tous vos fichiers ouverts.

2. Rendez-vous dans le dossier “TP1” que git vous a créé au départ.

3. Tapez la commande suivante pour ajouter tous les nouveaux fichiers :

        git add -A
        
	Ou pour ajouter juste un fichier :

        git add monfichier.cpp

4. Tapez la commande suivante pour créer la mise à jour :

        git commit -m "(votre message de mise à jour)"
        
5. Tapez la commande suivante pour envoyer la mise à jour :

        git push
        
- Si vous avez l’erreur `fatal: The current branch master has no upstream branch`, tapez à la place : `git push --set-upstream origin master`

- Si vous avez l’erreur `*** Please tell me who you are`, tapez `git config --global user.email "(votre adresse mail)"` et `git config --global user.name "(votre nom)"`, puis recommencez.
    
- Si jamais vous avez un message d’erreur du genre `Push rejected` : Appelez Matis

6. Faites une mise à jour :

        git pull

Quelques conseils…
------------------

### 1. **Évitez de travailler en même temps sur le même fichier !**

GIT utilise les numéros de ligne pour détecter les changements. Mais si deux personnes veulent mettre à jour le même fichier en même temps, GIT ne saura pas qui privilégier et vous aurez une erreur de conflit (dans ce cas, appeler Matis pour résoudre le problème...)

**Si c'est nécessaire de travailler en même temps sur le même fichier, appelez Matis pour avoir une explication du fonctionnement des _branches_ sous GIT...**

### 2. Faites des petites MàJ

Plutôt que d’implémenter 30 fonctions, ajouter 50 fichiers et modifier pleins de lignes avant de faire un commit, créez vos 50 fichiers, faites une màj; faites vos modifications de lignes fichier par fichier, faites une màj, etc.